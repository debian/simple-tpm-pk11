From: Ryan Trinkle <ryan.trinkle@gmail.com>
Date: Sun, 23 Dec 2018 00:00:35 +0000
Subject: Rework key enumeration

openssl (via opencryptoki) expects:
  * that valid handles are nonzero
  * to be able to retrieve different handles for the public and private keys within a keypair
  * to be able to enumerate them filtering on the CKA_CLASS of the key

This patch handles these issues by statically assigning the handles 1 and 2 to the public and private keys, respectively.
---
 src/session.cc | 36 +++++++++++++++++++++++++++---------
 src/session.h  |  2 ++
 2 files changed, 29 insertions(+), 9 deletions(-)

diff --git a/src/session.cc b/src/session.cc
index d79c9aa..4d87fa0 100644
--- a/src/session.cc
+++ b/src/session.cc
@@ -111,6 +111,13 @@ Config::read_file(std::ifstream& f)
   }
 }
 
+static
+CK_OBJECT_CLASS
+objectClass(CK_OBJECT_HANDLE hObject)
+{
+  return (hObject == 1) ? CKO_PUBLIC_KEY : CKO_PRIVATE_KEY;
+}
+
 Session::Session(const Config& config)
     :config_(config),
      findpos_(0)
@@ -127,21 +134,32 @@ Session::Login(CK_USER_TYPE type, const std::string& pin)
 void
 Session::FindObjectsInit(CK_ATTRIBUTE_PTR filters, int nfilters)
 {
-  findpos_ = 0;
+  findpos_ = 1; // Handles can't be 0, or cryptoki will interpret it as an error
+  filters_ = filters;
+  nfilters_ = nfilters;
 }
 
 int
 Session::FindObjects(CK_OBJECT_HANDLE_PTR obj, int maxobj)
 {
-  if (findpos_ == 1) {
-    return 0;
-  }
-  if (maxobj == 0) {
-    return 0;
+  int numFound = 0;
+  for(; numFound < maxobj && findpos_ <= 2; findpos_++) {
+    bool filterRejected = false;
+    for(int i = 0; i < nfilters_; i++) {
+      if(filters_[i].type == CKA_CLASS) {
+        if(*(CK_OBJECT_CLASS *)filters_[i].pValue != objectClass(findpos_)) {
+          filterRejected = true;
+          break;
+        }
+      } else {
+        // Ignore all other filters
+      }
+    }
+    if(!filterRejected) {
+      obj[numFound++] = findpos_;
+    }
   }
-  *obj = 0;
-  findpos_++;
-  return 1;
+  return numFound;
 }
 
 void
diff --git a/src/session.h b/src/session.h
index c39210b..7c00a9b 100644
--- a/src/session.h
+++ b/src/session.h
@@ -74,6 +74,8 @@ private:
   Config config_;
   std::string pin_;
   int findpos_;
+  CK_ATTRIBUTE_PTR filters_;
+  int nfilters_;
 };
 #endif
 /* ---- Emacs Variables ----
